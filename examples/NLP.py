
import os
import argparse
import sys
sys.path.append(".") # Adds higher directory to python modules path.
from src.FinalAnswer import FinalAnswer
from src.Bgcolors import Bgcolors
from src.naive_bayes_advance import NiaveBayes

def createDataFolder():
	if not os.path.exists('data'):
		os.makedirs('data')

def main():
	createDataFolder()

	parser = argparse.ArgumentParser()
	parser.add_argument('--mode',type=str,default ='chat',help='There are two mode (chat, train, train_c, test and none), The defaul value is chat.')
	# parser.add_argument("--benchmark", help="run benchmark",action="store_true")
	# parser.add_argument('--mode',type=float,default =0.2,help='There two mode?(train and chat)')
	args = parser.parse_args()

	config = {
		'dataset': 'dataset/db.json',
		'model'	 : 'data/naive_bayes_model.pickle',
		'mode'	 : 'nonunicode' # nonunicode or unicode
	}

	machine = NiaveBayes(**config)

	# -- mode 
	if args.mode == 'train' :
		machine.loadDataset()
		machine.train()
	elif args.mode == 'chat':
		machine.loadModel()
		print ("Start chatting with the bot !")
		while True:
			question 	= input('')
			answer 		= FinalAnswer().answer(machine.predict(question))
			print(Bgcolors().OKGREEN + 'You :' + Bgcolors().ENDC,question)
			print(Bgcolors().OKBLUE  + 'Bot :' + Bgcolors().ENDC,answer)


main()


