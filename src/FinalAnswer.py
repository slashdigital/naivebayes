from random import choice

class FinalAnswer(object):
    
    def __init__(self, *args, **kwargs):
        self.kwargs = kwargs

    # Naivae Bayes Algorithm
    def answer(self,c):
        answers = {
            'greetings_x'     : ['ចាស​ សួស្ដី!'],
            'goodbye_x'       : ['ជំរាប់លាចាស'],
            'name_x'          : ['ខ្ញុំឈ្មោះជាតា​ ត្រូវបានបង្កើតឡើងដោយក្រុមហ៊ុន​ Slash'],
            'age_x'           : ['ខ្ញុំទើបតែមានអាយុ១៥ថ្ងៃតែប៉ុណ្ណោះ'],
            'relationship_x'  : ['ខ្ញុំមិនទាន់មានសង្សារនៅឡើយទេ​ អ្នកអាចដាកពាក្យបាន'],
            'holiday_x'       : ['ចាស់មាន ថ្ងៃ១០ខែនេះជាថ្ងៃសិទ្ធមនុស្ស'],
            'budha_day_x'     : ['ថ្ងៃទី ៣ ១១ ១៧ ២៥ ជាថ្ងៃសិល'],
            'function_x'      : ['no answer'],
            'embassy_x'      : ['no answer'],
            'price_x'      : ['no answer'],
            'need_x'      : ['no answer'],
            'procedure_x'      : ['no answer'],
            'time_x'      : ['no answer'],
            'type_x'      : ['no answer'],
            'thanks_x'      : ['no answer'],
        } 

        return choice(answers[c])

