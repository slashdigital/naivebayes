import json
import nltk
from nltk.stem.lancaster import LancasterStemmer
stemmer = LancasterStemmer()
import math 
import operator

def sumWordofClasses(frequency):
	sumWords = dict()
	for c in frequency:
		for word in frequency[c]:
			if word not in sumWords: sumWords[word] = int()
			sumWords[word] += frequency[c][word]
	# print('word',sumWords)
	return sumWords
				
		
def loadDataset(file):
	with open(file) as data:
		training_data = json.load(data)
	words,documents,frequency,prob_of_class,classes,dataset_size = createDictionary(training_data)

	return words,documents,frequency,prob_of_class,classes,dataset_size

def cleanSentence(sentence):
	ignore_words 	= ['?']
	# tokenize each word in the sentence
	words = nltk.word_tokenize(sentence)
	words = [stemmer.stem(w.lower()) for w in words if w not in ignore_words]
	return words

def createDictionary(training_data):
	words 			= []
	classes 		= []
	documents 		= []
	ignore_words 	= ['?']
	frequency 		= dict()
	prob_of_class 	= dict()
	dataset_size 	= dict()
	# loop through each sentence in our training data
	for pattern in training_data['intents']:
		for intent in pattern['sentence']:
			if pattern['class'] not in dataset_size:
				dataset_size[pattern['class']] = int()
			dataset_size[pattern['class']] += 1
			# initial array frequency of class frequency['greetings']
			if pattern['class'] not in frequency:
				frequency[pattern['class']] = dict()
			# initial array frequency of class frequency['greetings']
			if pattern['class'] not in prob_of_class:
				prob_of_class[pattern['class']] = int()
			#frequency[pattern['class']][word] += 1
			prob_of_class[pattern['class']] += 1
			# clean sentence
			w = cleanSentence(intent)
			# count number of frequency of word
			for word in w:
				if word in frequency[pattern['class']]:
					frequency[pattern['class']][word] += 1
				else:
					frequency[pattern['class']][word] = 1
			# add to our words list
			words.extend(w)
		
			# add to documents in our corpus
			documents.append((w, pattern['class']))
			# add to our classes list
			if pattern['class'] not in classes:
				classes.append(pattern['class'])
	
	print('========== frequency ==========')
	print('frequency',frequency)

	print('========== prob_of_class ==========')
	print('prob_of_class',prob_of_class)

	print('========== original word ==========')
	print(words)

	print('========== dataset_size ==========')
	print('dataset_size',dataset_size)

	# stem and lower each word and remove duplicates
	words = [stemmer.stem(w.lower()) for w in words if w not in ignore_words]
	words = list(set(words))
	# print('========== after stem ==========')
	# print('words',words)

	return words,documents,frequency,prob_of_class,classes,dataset_size
	

def calP_Class_X(frequency,dataset_size,c,words):
	result = 1
	for word in words:
		if word in frequency[c]:
			# print(c,' frequency[c]',frequency[c][word],'dataset_size[c]',dataset_size[c])
			result *= frequency[c][word]/dataset_size[c]
		else:
			result *= 0.1 # alpha
	return result 

def calP_X(sum_words,dataset_size,c,words):
	result = 1
	sum_dataset_size = calTotalDataset(dataset_size)
	for word in words:
		if word in sum_words:
			# print('frequency[c][word]',frequency[c][word],'dataset_size[c]',dataset_size[c])
			result *= sum_words[word]/sum_dataset_size
	return result

def calTotalDataset(dataset_size):
	sum_dataset_size = 0
	for size in dataset_size:
		sum_dataset_size += dataset_size[size]	
	return sum_dataset_size

def classify(words,documents,frequency,prob_of_class ,classes,dataset_size,sentence):
	sum_words 	= sumWordofClasses(frequency)
	words 		= cleanSentence(sentence)
	sum_dataset_size = calTotalDataset(dataset_size)
	p = dict()
	for c in classes:
		# P(Class | X) 
		p_class_x = calP_Class_X(frequency,dataset_size,c,words)
		# P(Class)
		p_class = prob_of_class[c]/sum_dataset_size
		# P(X)	
		p_x = calP_X(sum_words,dataset_size,c,words)
		# print(' == ', p_class_x,p_class,p_x)
		p[c + '_x'] = p_class_x * p_class / p_x

	return p
		

words,documents,frequency,prob_of_class,classes,dataset_size = loadDataset('dataset/db.json')


# classify
benchmarks = [	"how much does it cost?",
				"What time is it",
				"How much does it cost",
				"what is an embassy",
				"Where is the place to make my visa",
				"Will it cost more than 50 dollar",
				"How much will it cost me",
				"What is the mean time to do my visa",
				"How long do I need to make my visa",
				"Can you help me finding the right visa",
				"how long will it takes and How much will it costs me?"
			]

print('========== benchmarks ==========')

for benchmark in benchmarks : 
	p = classify(words,documents,frequency ,prob_of_class,classes,dataset_size,benchmark)
	p = sorted(p.items(), key=operator.itemgetter(1),reverse=True)
	print('=== sentence ===> ' ,benchmark)
	print(p)